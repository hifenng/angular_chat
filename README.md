# ngular仿微信聊天模板，angular即时通讯模板，angular8模板源码

### 介绍
angular仿微信聊天模板，angular即时通讯模板，angular8模板源码,angular仿微信聊天|angular手机端IM聊天室|仿微信界面|angular聊天

### 软件架构
MVVM框架：angular8 + @angular/cli + @angular/router
状态管理：@ngrx/store + rxjs
地址路由：@angular/router
弹窗组件：wcPop
打包工具：webpack 2.0
环境配置：node.js + cnpm
图片预览：previewImage
轮播滑动：swiper

### 安装教程
	①：要确认你本地有node.js环境，同时安装了angular相关，如果你这方面经验很弱，请先简单了解ng的入门https://www.angular.cn/start
	②：到根目录下执行npm install命令，会自动生成node_modules文件夹，如果长时间没反应请删除node_modules文件再次执行npm install
	③：执行完之后直接运行 ng serve --port 8082 --open
使用说明
	如果第一次运行报错可能与ng版本迭代有关，请实现对应的初始化方法即可
参与贡献

### 【演示效果】
这是打包之后上传服务器的ng效果，首次加载速度会有点慢，请耐心等待 http://www.bester.wang

### 【后期升级版本】
整合ionic4，支持打包双系统app，接近原生app的体验，真正的即时通讯，真正的聊天app，对任何想整合im的项目都是零侵入性，随时随地整合聊天系统到你的原有项目，就是这么简单

### 【后期后端im技术简介】
springboot+cloud，让开发更简单，微服务让分布式和集群更简单，支持不停机更新，基于业界标杆netty进行im核心开发，性能爆表

###【演示效果】
这是打包之后上传服务器的ng效果，首次加载速度会有点慢，加上我服务器配置很低，请耐心等待 http://www.bester.wang

### 【技术交流】
交流群 604655085(拒绝任何广告)
# AngularChatroom

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
