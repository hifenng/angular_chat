/*
 *  @tmpl 群聊模板
 */

import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import * as $ from 'jquery'
declare var wcPop: any
import '../../assets/js/previewImage.js'
declare var previewImage: any
import Swiper from 'swiper'
@Component({
    selector: 'app-groupchat',
    template: `
        <div class="wcim__innerPage flexbox flex__direction-column">
            <!--//顶部-->
            <div class="wcim__topBar">
                <div class="inner flexbox flex-alignc">
                    <a class="linkico wcim__ripple-fff" (click)="routeBack()"><i class="iconfont icon-back"></i></a><h2 class="barTit sm flex1">
                        <div class="barCell flexbox flex__direction-column"><em class="clamp1">Angular交流群</em></div>
                    </h2> <a routerLink="/chat/group-info" class="linkico wcim__ripple-fff"><i class="iconfont icon-qunzu fs-40"><em class="wcim__badge wcim__badge-dot"></em></i></a>
                </div>
            </div>
            <div class="wcim__innerScroll flex1">
                <div class="chatMsg-cnt">
                    <ul class="clearfix" id="J__chatMsgList">
                        <li class="time"><span>昨天 23:15</span></li>
                        <li class="notice"><span>欢迎 John 加入本群</span></li>
                        <li class="notice"><span>当前群聊人数较多，请各位自觉修改备注名</span></li>
                        <li class="time"><span>07月15日 23:55</span></li>
                        <li class="others">
                            <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img04.jpg" /></a> <div class="content"> <p class="author">龙哥</p> <div class="msg">
                                    请各位好友遵守群规则发言，多次警告无效踢出聊天室，并自觉修改备注名。
                                </div>
                            </div>
                        </li>
                        <!--自己-->
                        <li class="me">
                            <div class="content"><p class="author">北鼻（Angle）</p> <div class="msg"> 欧喇，某文忒鸭~
                                </div>
                            </div>
                            <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></a>
                        </li>
                        <li class="others">
                            <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img10.jpg" /></a>
                            <div class="content"> <p class="author">Kiss（小布点 ▪ ㈱）</p>
                                <div class="msg picture"> <img class="img__pic" src="../../assets/img/placeholder/wchat__img03.jpg" />
                                </div> </div>
                        </li>
                        <li class="others">
                            <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img12.jpg" /></a>
                            <div class="content"> <p class="author">Flowers（杨迪）</p> <div class="msg"> 基于Angular全家桶技术(angular+angular-cli+angular-router+ngrx)。 <img class="face" src="../../assets/img/emotion/face01/63.png" />
                                </div>
                            </div>
                        </li>
                        <li class="time"><span>07月16日 早上08:25</span></li>
                        <li class="others">
                            <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img04.jpg" /></a> <div class="content">
                                <p class="author">龙哥</p> <div class="msg">
                                    “微信小程序官方文档”<br />
                                    <a href="https://mp.weixin.qq.com/cgi-bin/wx">https://mp.weixin.qq.com/cgi-bin/wx</a>
                                </div>
                            </div>
                        </li>
                        <li class="me">
                            <div class="content">
                                <p class="author">北鼻（Angle）</p> <div class="msg">
                                    <img class="face" src="../../assets/img/emotion/face01/77.png" />
                                    <img class="face" src="../../assets/img/emotion/face01/77.png" />
                                </div>
                            </div> <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></a>
                        </li>
                        <li class="me">
                            <div class="content"> <p class="author">北鼻（Angle）</p> <div class="msg video">
                                    <img class="img__video" src="../../assets/img/placeholder/wchat__video01-poster.jpg" videourl="../../assets/img/placeholder/wchat__video01-Y7qk5uVcNcFJIY8O4mKzDw.mp4" />
                                </div></div>
                            <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></a>
                        </li> <li class="time"><span>07月16日 下午15:30</span></li>
                        <li class="others">
                            <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img12.jpg" /></a>
                            <div class="content"> <p class="author">Flowers（杨迪）</p> <div class="msg video">
                                    <img class="img__video" src="../../assets/img/placeholder/wchat__video02-poster.jpg" videourl="../../assets/img/placeholder/wchat__video02-Y7qk5uVcNcFJIY8O4mKzDw.mp4" />
                                </div> </div>
                        </li>
                        <li class="time"><span>"Flowers（杨迪）" 撤回了一条消息</span></li> <li class="others">
                            <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img03.jpg" /></a><div class="content">
                                <p class="author">布鲁斯-科比</p><div class="msg lgface"> <img class="lg-face" src="../../assets/img/emotion/face02/14.gif" />
                                </div>
                            </div>
                        </li><li class="me"><div class="content">
                                <p class="author">北鼻（Angle）</p><div class="msg picture">
                                    <img class="img__pic" src="../../assets/img/placeholder/wchat__img02.jpg" />
                                </div>
                            </div> <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></a>
                        </li><li class="me">
                            <div class="content"><p class="author">北鼻（Angle）</p><div class="msg">
                                    有木有好的angular项目推荐下鸭！
                                </div>
                            </div><a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></a>
                        </li> </ul>
                </div> <div class="chatMsg-notice" id="J__chatMsgNotice"><i class="iconfont icon-gonggao fs-28"></i> 14条新消息</div>
            </div>

            <div class="wc__footToolbar">
                <div class="wc__editor-panel wcim__borT flexbox"> <div class="wrap-editor flex1"><div class="editor J__wcEditor" contentEditable="true" style="user-select:text;-webkit-user-select:text;"></div></div>
                    <a class="btn btn-emotion"><i class="iconfont icon-face"></i></a><a class="btn btn-choose"><i class="iconfont icon-choose"></i></a> <button class="btn-submit J__wchatSubmit"><i class="iconfont icon-submit"></i></button>
                </div>
                <div class="wc__choose-panel wcim__borT" style="display: none;"> <div class="wrap-emotion" style="display: none;">
                        <div class="emotion__cells flexbox flex__direction-column"> <div class="emotion__cells-swiper flex1" id="J__swiperEmotion">
                                <div class="swiper-container"> <div class="swiper-wrapper"></div> <div class="pagination-emotion"></div>
                                </div>
                            </div>
                            <div class="emotion__cells-footer" id="J__emotionFootTab">
                                <ul class="clearfix"> <li class="swiperTmpl cur" tmpl="swiper__tmpl-emotion01"><img src="../../assets/img/emotion/face01/face-lbl.png" /></li> <li class="swiperTmpl" tmpl="swiper__tmpl-emotion02"><img src="../../assets/img/emotion/face02/face-lbl.gif" /></li>
                                    <li class="swiperTmpl" tmpl="swiper__tmpl-emotion03"><img src="../../assets/img/emotion/face03/face-lbl.gif" /></li>
                                    <li class="swiperTmpl" tmpl="swiper__tmpl-emotion04"><img src="../../assets/img/emotion/face04/face-lbl.gif" /></li> <li class="swiperTmpl" tmpl="swiper__tmpl-emotion05"><img src="../../assets/img/emotion/face05/face-lbl.gif" /></li>
                                    <li class="swiperTmpl" tmpl="swiper__tmpl-emotion06"><img src="../../assets/img/emotion/face06/face-lbl.gif" /></li>
                                    <li class="swiperTmplSet"><img src="../../assets/img/wchat/icon__emotion-set.png" /></li> </ul>
                            </div>
                        </div>
                    </div>
                    <div class="wrap-choose" style="display: none;"> <div class="choose__cells"> <ul class="clearfix"> <li><a class="J__wchatZp" href="javascript:;"><span class="img"><img src="../../assets/img/wchat/icon__choose-zp.png" /><input type="file" accept="image/*" id="J__choosePicture" /></span><em>照片</em></a></li>
                                <li><a class="J__wchatSp" href="javascript:;"><span class="img"><img src="../../assets/img/wchat/icon__choose-sp.png" /><input type="file" accept="video/*" /></span><em>视频</em></a></li> <li><a class="J__wchatDs" href="javascript:;"><span class="img"><img src="../../assets/img/wchat/icon__choose-ds.png" /></span><em>打赏</em></a></li>
                                <li><a class="J__wchatHb" href="javascript:;"><span class="img"><img src="../../assets/img/wchat/icon__choose-hb.png" /></span><em>红包</em></a></li> <li><a class="J__wchatSc" href="javascript:;"><span class="img"><img src="../../assets/img/wchat/icon__choose-sc.png" /></span><em>我的收藏</em></a></li>
                                <li><a class="J__wchatWj" href="javascript:;"><span class="img"><img src="../../assets/img/wchat/icon__choose-wj.png" /></span><em>文件</em></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper__tmpl-emotion01" style="display: none;">
                <div class="swiper-slide">
                    <div class="face-list face__sm-list">
                        <span><img class="face" src="../../assets/img/emotion/face01/0.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/1.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/2.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/3.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/4.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/5.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/6.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/7.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/8.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/9.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/10.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/11.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/12.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/13.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/14.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/15.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/16.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/17.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/18.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/19.png" /></span>
                        <span><img class="del" src="../../assets/img/wchat/icon__emotion-del.png" /></span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="face-list face__sm-list">
                        <span><img class="face" src="../../assets/img/emotion/face01/20.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/21.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/22.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/23.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/24.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/25.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/26.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/27.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/28.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/29.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/30.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/31.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/32.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/33.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/34.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/35.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/36.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/37.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/38.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/39.png" /></span>
                        <span><img class="del" src="../../assets/img/wchat/icon__emotion-del.png" /></span>
                    </div>
                </div> <div class="swiper-slide">
                    <div class="face-list face__sm-list"> <span><img class="face" src="../../assets/img/emotion/face01/40.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/41.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/42.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/43.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/44.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/45.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/46.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/47.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/48.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/49.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/50.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/51.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/52.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/53.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/54.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/55.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/56.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/57.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/58.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/59.png" /></span>
                        <span><img class="del" src="../../assets/img/wchat/icon__emotion-del.png" /></span> </div>
                </div>
                <div class="swiper-slide"> <div class="face-list face__sm-list">
                        <span><img class="face" src="../../assets/img/emotion/face01/60.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/61.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/62.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/63.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/64.png" /></span> <span><img class="face" src="../../assets/img/emotion/face01/65.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/66.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/67.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/68.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/69.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/70.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/71.png" /></span> <span><img class="face" src="../../assets/img/emotion/face01/72.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/73.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/74.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/75.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/76.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/77.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/78.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/79.png" /></span>
                        <span><img class="del" src="../../assets/img/wchat/icon__emotion-del.png" /></span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="face-list face__sm-list">
                        <span><img class="face" src="../../assets/img/emotion/face01/80.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/81.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/82.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/83.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/84.png" /></span>
                        <span><img class="face" src="../../assets/img/emotion/face01/85.png" /></span>
                        <span><img class="del" src="../../assets/img/wchat/icon__emotion-del.png" /></span>
                    </div>
                </div>
            </div>
            <div class="swiper__tmpl-emotion02" style="display: none;">
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/0.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/1.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/2.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/3.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/4.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/5.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/6.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/7.gif" /></span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/8.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/9.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/10.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/11.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/12.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/13.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/14.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face02/15.gif" /></span>
                    </div>
                </div>
            </div>
            <div class="swiper__tmpl-emotion03" style="display: none;">
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/0.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/1.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/2.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/3.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/4.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/5.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/6.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/7.gif" /></span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/8.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/9.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/10.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/11.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/12.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/13.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/14.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face03/15.gif" /></span>
                    </div>
                </div>
            </div>
            <div class="swiper__tmpl-emotion04" style="display: none;">
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/0.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/1.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/2.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/3.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/4.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/5.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/6.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/7.gif" /></span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/8.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/9.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/10.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/11.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/12.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/13.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/14.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face04/15.gif" /></span>
                    </div>
                </div>
            </div>
            <div class="swiper__tmpl-emotion05" style="display: none;">
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/0.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/1.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/2.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/3.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/4.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/5.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/6.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/7.gif" /></span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/8.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/9.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/10.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/11.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/12.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/13.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/14.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face05/15.gif" /></span>
                    </div>
                </div>
            </div>
            <div class="swiper__tmpl-emotion06" style="display: none;">
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/0.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/1.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/2.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/3.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/4.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/5.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/6.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/7.gif" /></span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="face-list face__lg-list">
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/8.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/9.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/10.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/11.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/12.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/13.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/14.gif" /></span>
                        <span><img class="lg-face" src="../../assets/img/emotion/face06/15.gif" /></span>
                    </div>
                </div>
            </div>

            <!--//打赏模板-->
            <div class="wcim__popup-tmpl">
                <div id="J__popupTmpl-Dashang" style="display: none;">
                    <div class="wcim__popupTmpl tmpl-hongbao tmpl-dashang">
                        <h2 class="labelTips">为您喜欢的人霸屏打赏</h2>
                        <ul class="clearfix">
                            <li><div class="item flexbox"><span class="lbl">打赏人</span><input class="ipt-txt flex1" type="tel" name="hbTotal" placeholder="填写打赏人" value="大幂幂" readonly /></div></li>
                            <li><div class="item flexbox"><input class="ipt-txt flex1" type="text" name="content" placeholder="输入打赏语，30字以内（选填）" style="text-align:left;" /></div></li>
                            <li>
                                <div class="item item-gift" id="J__chooseGift">
                                    <div class="gift flexbox wcim__material-cell">
                                        <label class="txt"><span>跑车</span><em class="time">霸屏50秒</em></label> <span class="amount">￥<em>29</em> <i class="chkbox"></i></span>
                                    </div>
                                    <div class="gift flexbox wcim__material-cell selected">
                                        <label class="txt"><span>动人玫瑰</span><em class="time">霸屏20秒</em></label> <span class="amount">￥<em>8</em> <i class="chkbox"></i></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--//红包模板-->
            <div class="wcim__popup-tmpl">
                <div id="J__popupTmpl-Hongbao" style="display: none;">
                    <div class="wcim__popupTmpl tmpl-hongbao">
                        <ul class="clearfix">
                            <li>
                                <div class="item flexbox"><span class="lbl">总金额</span><input class="ipt-txt flex1" type="tel" name="hbTotal" placeholder="0.00" value="168" /><em class="unit">元</em></div>
                            </li>
                            <li>
                                <div class="item flexbox"><span class="lbl">红包个数</span><input class="ipt-txt flex1" type="tel" name="hbNum" placeholder="填写个数" value="10" /><em class="unit">个</em></div>
                                <div class="memTips">在线人数共<em class="count">168</em>人</div>
                            </li>
                            <li><div class="item flexbox"><span class="lbl">留言</span><input class="ipt-txt flex1" type="text" name="content" placeholder="恭喜发财，大吉大利" /></div></li>
                            <li><div class="moneyTotal"> ¥ <em class="num">168.00</em></div></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    `,
    styles: [``]
})
export class GroupChatComponent implements OnInit {
    constructor(
        private router: Router
    ) { }

    ngOnInit(): void {
        let _that = this
        function wchat_ToBottom() { $(".wcim__innerScroll").animate({ scrollTop: $("#J__chatMsgList").height() }, 0);
        }
        $(document).on("click", ".wcim__innerScroll", function (e) {
            var _tapMenu = $(".wc__chatTapMenu");if (_tapMenu.length && e.target != _tapMenu && !$.contains(_tapMenu[0], e.target)) {
                _tapMenu.hide();
                $(".wcim__innerScroll").find("li .msg").removeClass("taped");
            }
            $(".wc__choose-panel").hide();
        });

        $("body").on("click", ".wc__editor-panel .btn", function () {
            var that = $(this);
            $(".wc__choose-panel").show();
            if (that.hasClass("btn-emotion")) {
                $(".wc__choose-panel .wrap-emotion").show(); $(".wc__choose-panel .wrap-choose").hide();
                !emotionSwiper && $("#J__emotionFootTab ul li.cur").trigger("click");
            } else if (that.hasClass("btn-choose")) {
                $(".wc__choose-panel .wrap-emotion").hide(); $(".wc__choose-panel .wrap-choose").show();
            }
            wchat_ToBottom();
        });


        var emotionSwiper;
        function setEmotionSwiper(tmpl) {
            var _tmpl = tmpl ? tmpl : $("#J__emotionFootTab ul li.cur").attr("tmpl");
            $("#J__swiperEmotion .swiper-container").attr("id", _tmpl); $("#J__swiperEmotion .swiper-wrapper").html($("." + _tmpl).html());
            emotionSwiper = new Swiper('#' + _tmpl, {
                pagination: {el: '.pagination-emotion', clickable: true,
                },
            });
        }
        $("body").on("click", "#J__emotionFootTab ul li.swiperTmpl", function () {
            emotionSwiper && emotionSwiper.destroy(true, true);var _tmpl = $(this).attr("tmpl");$(this).addClass("cur").siblings().removeClass("cur");
            setEmotionSwiper(_tmpl);
        });


        $("body").on("click", "#J__chatMsgList li .video", function () {
            var _src = $(this).find("img").attr("videourl"), _video;
            var videoIdx = wcPop({
                id: 'wc__previewVideo',skin: 'fullscreen',content: '<video id="J__videoPreview" width="100%" height="100%" controls="controls" preload="auto"></video>',
				shade: false, xclose: true, style: 'background: #000;padding-top:48px;', anim: 'scaleIn',
                show: function(){
                    _video = document.getElementById("J__videoPreview");  _video.src = _src;
                    if (_video.paused) {  _video.play();
                    } else { _video.pause();
                    }
                    _video.addEventListener("ended", function(){  _video.currentTime = 0;
                    });
                    _video.addEventListener("x5videoexitfullscreen", function(){ wcPop.close(videoIdx);
                    })
                }
			});
        });


        function surrounds() {
            setTimeout(function () { //chrome
                var sel = window.getSelection();
                var anchorNode = sel.anchorNode;
                if (!anchorNode) return;
                if (sel.anchorNode === $(".J__wcEditor")[0] ||
                    (sel.anchorNode.nodeType === 3 && sel.anchorNode.parentNode === $(".J__wcEditor")[0])) {
                    var range = sel.getRangeAt(0);
                    var p = document.createElement("p");
                    range.surroundContents(p);
                    range.selectNodeContents(p);
                    range.insertNode(document.createElement("br")); //chrome
                    sel.collapse(p, 0);
                    (function clearBr() {
                        var elems = [].slice.call($(".J__wcEditor")[0].children);
                        for (var i = 0, len = elems.length; i < len; i++) {
                            var el = elems[i];
                            if (el.tagName.toLowerCase() == "br") {
                                $(".J__wcEditor")[0].removeChild(el);
                            }
                        }
                        elems.length = 0;
                    })();
                }
            }, 10);
        }
        var _lastRange = null, _sel = window.getSelection && window.getSelection();
        var _rng = {
            getRange: function () {
                if (_sel && _sel.rangeCount > 0) { return _sel.getRangeAt(0);
                }
            },
            addRange: function () {
                if (_lastRange) {
                    _sel.removeAllRanges(); _sel.addRange(_lastRange);
                }
            }
        }

        $("body").on("click", ".J__wcEditor", function(){ $(".wc__choose-panel").hide();
        });
        $("body").on("focus", ".J__wcEditor", function(){
            surrounds();
        });
        $("body").on("input", ".J__wcEditor", function(){
            surrounds();
        });

        $("body").on("click", "#J__swiperEmotion .face-list span img", function () {
            var that = $(this), range;
            if (that.hasClass("face")) { //小表情
                var img = that[0].cloneNode(true);
                if (!$(".J__wcEditor")[0].childNodes.length) {$(".J__wcEditor")[0].focus();
                }
                $(".J__wcEditor")[0].blur();
                setTimeout(function () {
                    if (window.getSelection && window.getSelection().getRangeAt) {
                        range = _rng.getRange();
                        range.insertNode(img);
                        range.collapse(false);
                        _lastRange = range; 
                        _rng.addRange();
                    }
                }, 10);
            } else if (that.hasClass("del")) {
                $(".J__wcEditor")[0].blur();
                setTimeout(function () {
                    range = _rng.getRange();
                    range.collapse(false);
                    document.execCommand("delete");
                    _lastRange = range;
                    _rng.addRange();
                }, 10);
            } else if (that.hasClass("lg-face")) { //大表情
                var _img = that.parent().html();
                var _tpl = [
                    '<li class="me">\
                        <div class="content">\
                            <p class="author">北鼻（Angle）</p> <div class="msg lgface">'+ _img + '</div>\
                        </div> <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></a>\
                    </li>'
                ].join("");
                $("#J__chatMsgList").append(_tpl);
                wchat_ToBottom();
            }
        });
        function isEmpty() {
            var html = $(".J__wcEditor").html();
            html = html.replace(/<br[\s\/]{0,2}>/ig, "\r\n");
            html = html.replace(/<[^img].*?>/ig, "");
            html = html.replace(/&nbsp;/ig, "");
            return html.replace(/\r\n|\n|\r/, "").replace(/(?:^[ \t\n\r]+)|(?:[ \t\n\r]+$)/g, "") == "";
        }
        $("body").on("click", ".J__wchatSubmit", function () {
            if (isEmpty()) return;
            var _html = $(".J__wcEditor").html();
            var reg = /(http:\/\/|https:\/\/)((\w|=|\?|\.|\/|&|-)+)/g;
            _html = _html.replace(reg, "<a href='$1$2' target='_blank'>$1$2</a>");
            var msgTpl = [
                '<li class="me"><div class="content"> <p class="author">北鼻（Angle）</p> <div class="msg">'+ _html + '</div>\
                    </div><a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></a>\
                </li>'
            ].join("");
            $("#J__chatMsgList").append(msgTpl);
            if (!$(".wcim__choose-panel").is(":hidden")) { $(".J__wcEditor").html("");
            } else { $(".J__wcEditor").html("").focus();
            }
            wchat_ToBottom();
        });


        // ...选择图片
        $("body").on("change", "#J__choosePicture", function () {
            $(".wcim__choose-panel").hide();
            var file = this.files[0];var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (e) {
                var _img = this.result; var _tpl = [
                    '<li class="me"> <div class="content"> <p class="author">北鼻（Angle）</p> <div class="msg picture"><img class="img__pic" src="'+ _img + '" preview="1" data-pswp-uid="4" /></div>\
                        </div>\
                        <a class="avatar" href="javascript:;"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></a>\
                    </li>'
                ].join("");
                $("#J__chatMsgList").append(_tpl);
                setTimeout(function () { wchat_ToBottom(); }, 17);
            }
        });

        $("body").on("click", "#J__chatMsgList li .picture", function(){
            var current = $(this).find("img").attr("src"), urls = []; $("#J__chatMsgList li .picture").each(function (i, item) { urls.push($(this).find("img").attr("src"));
            });
            var obj = { urls : urls, current : current
            }; previewImage.start(obj);
        });

        $("body").on("click", "li .avatar", function(e){ _that.router.navigate(['/contact/uinfo'])
        });
        $("body").on("contextmenu", "li .avatar", function(e){
            var author = $(this).siblings(".content").find(".author").text();
            var atidx = wcPop({ skin: 'actionsheet',
                content: '<span style="color:#aaa;">'+author+'</span>', anim: 'footer', shadeClose: true,
                btns: [
                    {
                        text: '@他/她', style: 'color:#45cff5 ;', onTap() {
                            $(".wc__choose-panel").hide();
                            var _html = '<em class="onez-at" data-at="1233" data-at-uname="@'+author+'">@'+author+'</em>&nbsp;';
                            $(".J__wcEditor").html("").append(_html);
                            $(".J__wcEditor")[0].focus();
                            var range = window.getSelection();
                            range.selectAllChildren($(".J__wcEditor")[0]); range.collapseToEnd();
                            wcPop.close(atidx);
                        }
                    },
                    { text: '取消', onTap() {
                            wcPop.close(atidx);
                        }
                    }
                ]
            });
        });

        $("body").on("click", ".J__wchatHb", function(){
            var hbidx = wcPop({
				skin: 'ios',content: $("#J__popupTmpl-Hongbao").html(),style: 'background-color: #f5f7fb; max-width: 320px; width: 90%;',
				xclose: true,
				btns: [
					{	text: '塞钱进红包',style: 'background:#378fe7;color:#fff;font-size:16px;',
						onTap() {	wcPop.close(hbidx);
						}
					}
				]
			});
        });

        $("body").on("click", ".J__wchatDs", function(){
            var dashangIdx = wcPop({ skin: 'ios', content: $("#J__popupTmpl-Dashang").html(), style: 'background-color: #f5f7fb; max-width: 320px; width: 90%;',
                xclose: true,
                btns: [
					{
						text: '<span class="btn-dashang">打赏</span>',style: 'background:#378fe7;color:#fff;font-size:16px;',onTap() {
							wcPop.close(dashangIdx);
						}
					}
				]
            });
        });

    }

    routeBack(){
        history.go(-1)
    }
}
