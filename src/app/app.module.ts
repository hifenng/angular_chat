import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module'

import { StoreModule } from '@ngrx/store'
import { reducer } from '../ngrx'

import { HeaderComponent } from '../components/header'
import { TabBarComponent } from '../components/tabbar'
import { XtnScroll } from '../components/xtnScroll/Scroll'
import { NotFoundComponent } from '../components/404'
import { AppComponent } from './app.component'
import { LoginComponent } from '../views/auth/login'
import { RegisterComponent } from '../views/auth/register'
import { IndexComponent } from '../views/index'
import { ContactComponent } from '../views/contact'
import { UinfoComponent } from '../views/contact/uinfo'
import { UcenterComponent } from '../views/ucenter'
import { GroupChatComponent } from '../views/chat/group-chat'
import { GroupInfoComponent } from '../views/chat/group-info'
import { SingleChatComponent } from '../views/chat/single-chat'

@NgModule({
  declarations: [
    HeaderComponent,
    TabBarComponent,
    XtnScroll,
    NotFoundComponent,

    AppComponent,
    LoginComponent,
    RegisterComponent,
    IndexComponent,
    ContactComponent,
    UinfoComponent,
    UcenterComponent,
    GroupChatComponent,
    GroupInfoComponent,
    SingleChatComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,

    StoreModule.forRoot(reducer)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
